import './style.css';

export class Link {
  constructor(el1, el2, linkId) {
    this._el1 = el1;
    this._el2 = el2;
    this._linkId = linkId;
    this._update();
    this.linkInstance = this._createLink();
  }

  connect() {
    document.body.insertAdjacentHTML('beforeend', this.linkInstance);
    this._update(false, this._linkId);
  }

  _update(init = true, linkId = null) {
    this._el1Props = this._el1.getBoundingClientRect();
    this._el2Props = this._el2.getBoundingClientRect();
    this._anchors = this._getAnchors();
    this._length = this._getLinkLength();
    this._angle = this._getLinkAngle();
    this._startPoint = this._getStartPoint();

    if (!init && linkId) {
      const currentLink = document.getElementById(linkId);

      currentLink.style.width = `${this._length}px`;

      currentLink.style.top = `${
        this._anchors[0].top + this._startPoint.yOffset + pageYOffset
      }px`;

      currentLink.style.left = `${
        this._anchors[0].left - this._startPoint.xOffset + pageXOffset
      }px`;

      currentLink.style.transform = `rotate(${this._angle}rad)`;

      requestAnimationFrame(this._update.bind(this, false, linkId));
    }
  }

  _getAnchors() {
    return [
      {
        left: this._el1Props.x + this._el1Props.width / 2,
        top: this._el1Props.y + this._el1Props.height / 2,
      },
      {
        left: this._el2Props.x + this._el2Props.width / 2,
        top: this._el2Props.y + this._el2Props.height / 2,
      },
    ];
  }

  _getLinkLength() {
    return Math.sqrt(
      Math.abs(
        Math.pow(this._anchors[1].left - this._anchors[0].left, 2) +
          Math.pow(this._anchors[1].top - this._anchors[0].top, 2)
      )
    );
  }

  _getLinkAngle() {
    const vectorCoords = {
      x: this._el2Props.x - this._el1Props.x,
      y: this._el2Props.y - this._el1Props.y,
    };

    return Math.atan2(vectorCoords.y, vectorCoords.x);
  }

  _getStartPoint() {
    return {
      xOffset: Math.round(this._length / 2 - Math.cos(this._angle) * (this._length / 2)),
      yOffset: Math.round(Math.sin(this._angle) * (this._length / 2)),
    };
  }

  _createLink() {
    return `
      <div 
          id=${this._linkId}
          class="link"
          style="
            width: ${this._length}px; 
            top: ${this._anchors[0].top + this._startPoint.yOffset + pageYOffset}px;
            left: ${this._anchors[0].left - this._startPoint.xOffset + pageXOffset}px;
            transform: rotate(${this._angle}rad);
          "
      ></div>
    `;
  }
}
