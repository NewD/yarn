import { Link } from './Link';
import { getNodeByAnySelector } from './helpers/getNodeByAnySelector';
import { createLinks } from './helpers/createLinks';

const links = [];

window.connectElements = (selector1, selector2, deep) => {
  const existedLink = links.find(
    link =>
      (link[0] === selector1 && link[1] === selector2) ||
      (link[1] === selector1 && link[0] === selector2)
  );

  if (!!existedLink) return;

  if (deep) {
    let elements;
    if (selector1 === selector2) {
      elements = getNodeByAnySelector(selector1, true);
    } else
      elements = [
        ...getNodeByAnySelector(selector1, true),
        ...getNodeByAnySelector(selector2, true),
      ];

    createLinks(elements);
  }

  const elem1 = getNodeByAnySelector(selector1);
  const elem2 = getNodeByAnySelector(selector2);

  const link = new Link(elem1, elem2, 'link1');
  link.connect();
  links.push([selector1, selector2]);
};
