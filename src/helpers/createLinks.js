import { Link } from '../Link';
import { v4 as uuidv4 } from 'uuid';

export function createLinks(elements) {
  console.log(elements[0]);
  for (let i = 0; i < elements.length; i++) {
    for (let j = 1; j < elements.length; j++) {
      const link = new Link(elements[i], elements[j], uuidv4());
      link.connect();
    }
  }
}
