export function getNodeByAnySelector(selector, deep) {
  if (deep) {
    return document.querySelectorAll(selector);
  }

  // if (selector.test(/^#/)) {
  //   return document.getElementById(selector);
  // }

  return document.querySelector(selector);
}
